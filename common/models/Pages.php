<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $robots
 * @property string $name
 * @property string $alias
 * @property integer $category_id
 * @property string $content
 * @property string $template
 * @property integer $active
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'category_id', 'content', 'template', 'active'], 'required'],
            [['category_id', 'active'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 25],
            [['description'], 'string', 'max' => 120],
            [['keywords'], 'string', 'max' => 150],
            [['robots'], 'string', 'max' => 2],
            [['name', 'alias', 'template'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'robots' => 'Robots',
            'name' => 'Name',
            'alias' => 'Alias',
            'category_id' => 'Category ID',
            'content' => 'Content',
            'template' => 'Template',
            'active' => 'Active',
        ];
    }
}
