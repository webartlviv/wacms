<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "general_page".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $content
 * @property integer $position
 * @property integer $active
 */
class GeneralPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'general_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'content', 'position', 'active'], 'required'],
            [['content'], 'string'],
            [['position', 'active'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'content' => 'Content',
            'position' => 'Position',
            'active' => 'Active',
        ];
    }
}
