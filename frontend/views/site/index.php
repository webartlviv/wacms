<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="page">

<header class="header header_white">
        <div class="header__holder">
            <div class="header__line">
                <a href="javascript:history.back()" class="back-button">
                    <div class="back-button__holder"><i class="back-button__icon"></i> </div>
                </a>
                <div class="menu-button js-menu-button">
                    <div class="menu-button__holder">
                        <div class="menu-button__line menu-button__line_top"></div>
                        <div class="menu-button__line menu-button__line_middle"></div>
                        <div class="menu-button__line menu-button__line_bottom"></div>
                    </div>
                </div>
                <div class="logo">
                    <a class="logo__link">
                        <svg viewbox="0 0 195 35" xml:space="preserve" class="logo__image">
                        <use xlink:href="#logo"></use>
                        </svg>
                    </a>
                </div>
                <div class="header__right-panel">
                    <a href="/user/wishlist" class="favorite-button">
                        <i class="favorite-button__icon">
                            <span class="favorite-button__count" style="display: none">0</span> </i>
                    </a>
                    <a href="/cart" class="basket-button">
                        <i class="basket-button__icon">
                            <span class="basket-button__count" style="display: none">0</span> </i>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <main class="main">
        <div class="full-page">
            <ul class="full-page__nav js-full-page-nav">
                <?php $count = count($items);
                    for( $i= 0; $i < $count; $i++ ) { ?>
                <li class="full-page__nav-item">
                    <a href="#<?= $i ?>" class="full-page__nav-link"></a>
                </li>
                <?php } ?>
            </ul>
            <div class="full-page__scroll js-full-page">
                <?php foreach ($items as $item) { ?>
                    <div class="full-page__item">
                        <div class="full-page__slider js-full-page-slider">
                            <div style="background-image: url('<?= $item->image ?>')" class="full-page__view full-page__header_white"></div>
                        </div>
                        <div class="full-page__holder">
                            <div class="full-page__box">
                                <?=  $item->content ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </main>

<footer class="footer footer_white" id="footer">
    <div class="footer__holder">
        <div class="footer__line">
            <div class="footer__left">
                <div class="reference-tools">
                    <div class="reference-tools__item"> <a href="//en.12storeez.com/" class="reference-tools__link en-US">
                            <i class="reference-tools__icon reference-tools__icon_lang"></i>English version </a>
                    </div> <div class="reference-tools__item"><a href="/about/otzyvy" class="reference-tools__link"><i class="reference-tools__icon reference-tools__icon_reviews"></i>Отзывы</a></div>
                </div>
            </div>
            <div class="footer__middle">
                <div class="social-icons">
                    <div class="social-icons__item instagram"><a href="http://instagram.com/12Storeez" class="social-icons__link" target="_blank">instagram<i class="social-icons__icon social-icons__icon_instagram"></i></a></div>
                    <div class="social-icons__item facebook"><a href="https://www.facebook.com/12StoreezShop" class="social-icons__link" target="_blank">facebook<i class="social-icons__icon social-icons__icon_facebook"></i></a></div>
                    <div class="social-icons__item vkontakte"><a href="http://vk.com/12storeezshop" class="social-icons__link" target="_blank">vkontakte<i class="social-icons__icon social-icons__icon_vkontakte"></i></a></div>
                </div>
            </div>
            <div class="footer__right">
                <div class="reference-tools">
                    <div class="reference-tools__item"><a id="shops-link" href="/about/shops" class="reference-tools__link"><i class="reference-tools__icon reference-tools__icon_stores"></i>Магазины</a></div>
                    <div class="reference-tools__item"><a id="contacts-link" href="/about/contacts" class="reference-tools__link"><i class="reference-tools__icon reference-tools__icon_contacts"></i>Контакты</a></div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>