<div class="page">
    <header class="header ">
        <div class="header__holder">
            <div class="header__line">
                <a href="javascript:history.back()" class="back-button">
                    <div class="back-button__holder"><i class="back-button__icon"></i> </div>
                </a>
                <div class="menu-button js-menu-button">
                    <div class="menu-button__holder">
                        <div class="menu-button__line menu-button__line_top"></div>
                        <div class="menu-button__line menu-button__line_middle"></div>
                        <div class="menu-button__line menu-button__line_bottom"></div>
                    </div>
                </div>
                <div class="logo">
                    <a href="/" class="logo__link">
                        <svg viewbox="0 0 195 35" xml:space="preserve" class="logo__image">
                  <use xlink:href="#logo"></use>
                </svg>
                    </a>
                </div>
                <div data-url="" data-uptitle="вверх" class="header__title js-scroll-top">О компании</div>
                <div class="header__right-panel">
                    <a href="/user/wishlist" class="favorite-button">
                        <i class="favorite-button__icon">
                            <span class="favorite-button__count" style="display: none">0</span>                </i>
                    </a>
                    <a href="/cart" class="basket-button">
                        <i class="basket-button__icon">
                            <span class="basket-button__count" style="display: none">0</span>                  </i>
                    </a>
                </div>
            </div>
        </div>
    </header>

    <main class="main">
        <div class="blog-post">
            <div class="blog-post__holder">
                <div class="page__row">
                    <!-- <div class="page__sidebar page__sidebar_left">
                      <a href="#" class="page-back">
                        <div class="page-back__holder"><i class="page-back__icon"></i><span class="page-back__text">Блог</span> </div>
                      </a>
                    </div> -->
                    <div class="page__middle">
                        <div class="blog-post__top">
                            <div class="blog-post__header">
                                <h1 class="blog-post__title"><?php echo $page->name; ?></h1>
                                <div class="blog-post__date">&nbsp;</div>
                            </div>
                        </div>
                        <div class="content blog-post__content"><?php echo $page->content; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="footer " id="footer">
        <div class="footer__holder">
            <div class="page__row">
                <div class="subscribe footer__subscribe media-query-show_mobile">
                    <h2 class="subscribe__head">узнайте первыми о&nbsp;новинках и&nbsp;скидках</h2>

                    <form id="subscribe-form-mobile" class="subscribe-form subscribe-form_gray" action="/subscribe" method="post">
                        <div class="form-group field-subscriber-source">

                            <input type="hidden" id="subscriber-source" class="form-control" name="Subscriber[source]" value="widget">

                            <div class="help-block"></div>
                        </div>

                        <div class="form__item field-subscriber-email required">
                            <input type="text" id="subscriber-email" class="subscribe-form__input" name="Subscriber[email]" maxlength="255" placeholder="укажите e-mail" autocomplete="off" aria-required="true"><div class="form__error-message"></div>
                        </div>
                        <button type="submit" class="button subscribe-form__button">Подписаться</button>
                    </form>
                    <div style="display:none">
                        <div id="thank_you" class="popup popup_small">
                            <div class="popup__holder">
                                <h4 class="popup__head">Спасибо!</h4>
                                <p>Мы будем сообщать вам о новинках.</p>
                                <button type="button" class="button button_powdery featherlight-close">Продолжить</button>
                            </div>
                        </div>
                    </div>            <div class="subscribe__icons-wrapper">
                        <div class="social-icons">
                            <div class="social-icons__item"><a href="http://instagram.com/12Storeez" class="social-icons__link" target="_blank">instagram<i class="social-icons__icon social-icons__icon_instagram"></i></a></div>
                            <div class="social-icons__item"><a href="https://www.facebook.com/12StoreezShop" class="social-icons__link" target="_blank">facebook<i class="social-icons__icon social-icons__icon_facebook"></i></a></div>
                            <div class="social-icons__item"><a href="http://vk.com/12storeezshop" class="social-icons__link" target="_blank">vkontakte<i class="social-icons__icon social-icons__icon_vkontakte"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="page__middle">
                    <div class="copyright copyright_mobile">
                        <svg viewbox="0 0 195 35" xml:space="preserve" class="copyright__logo">
                    <use xlink:href="#logo"></use>
                  </svg>
                    </div>
                    <nav class="bottom-menu media-query-show_mobile">
                        <ul class="bottom-menu__list">
                            <li class="bottom-menu__item">
                                <a href="//en.12storeez.com/about" class="bottom-menu__link">
                                    English version		</a>
                            </li>

                            <li class="bottom-menu__item"><a href="/pages/dostavka" class="bottom-menu__link">Условия доставки</a> </li>
                            <li class="bottom-menu__item"><a href="/pages/besplatnyiy-vozvrat" class="bottom-menu__link">Бесплатный возврат</a> </li>
                            <li class="bottom-menu__item"><a href="/pages/oplata" class="bottom-menu__link">Оплата</a> </li>
                            <li class="bottom-menu__item"><a href="/pages/faq" class="bottom-menu__link">Вопросы и ответы</a> </li>
                            <li class="bottom-menu__item"><a href="/pages/otzyvy" class="bottom-menu__link">Отзывы о магазине</a> </li>
                        </ul>
                    </nav>
                    <nav class="bottom-menu media-query-show_tablet">
                        <ul class="bottom-menu__list">
                            <li class="bottom-menu__item">
                                <a href="/pages" class="bottom-menu__link">О компании</a>
                                <ul class="bottom-menu__sub">
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/otzyvy" class="bottom-menu__sub-link">отзывы</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/brend" class="bottom-menu__sub-link">о нас</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/press" class="bottom-menu__sub-link">пресса о нас</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/shops" class="bottom-menu__sub-link">магазины</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/contacts" class="bottom-menu__sub-link">контакты</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/rabota-u-nas" class="bottom-menu__sub-link">работа у нас</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/office" class="bottom-menu__sub-link">офис</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="bottom-menu__item">
                                <a href="/info" class="bottom-menu__link">Покупателям</a>
                                <ul class="bottom-menu__sub">
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/samovyvoz" class="bottom-menu__sub-link">самовывоз</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/politika-konfidencialnosti" class="bottom-menu__sub-link">политика конфиденциальности</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/rukovodstvo-po-pokupke" class="bottom-menu__sub-link">руководство по покупке</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/oplata" class="bottom-menu__sub-link">оплата</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/dostavka" class="bottom-menu__sub-link">доставка</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/besplatnyiy-vozvrat" class="bottom-menu__sub-link">бесплатный возврат</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/faq" class="bottom-menu__sub-link">часто задаваемые вопросы (F.A.Q.)</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/kacestvo" class="bottom-menu__sub-link">качество обслуживания</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/oferta" class="bottom-menu__sub-link">оферта</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/pages/idei" class="bottom-menu__sub-link">идеи и предложения</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="bottom-menu__item">
                                <a href="/user/cabinet" class="bottom-menu__link">Мой кабинет</a>
                                <ul class="bottom-menu__sub">
                                    <li class="bottom-menu__sub-item">
                                        <a href="/user/cabinet" class="bottom-menu__sub-link">профиль</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/user/orders" class="bottom-menu__sub-link">мои заказы</a>
                                    </li>
                                    <li class="bottom-menu__sub-item">
                                        <a href="/user/wishlist" class="bottom-menu__sub-link">wishlist</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="bottom-menu__item bottom-menu__item_subscribe">
                                <div class="bottom-menu__title">узнайте первыми о&nbsp;новинках и&nbsp;скидках</div>

                                <form id="subscribe-form-desktop" class="subscribe-form subscribe-form_gray subscribe-form_left" action="/subscribe" method="post">
                                    <div class="form-group field-subscriber-source">

                                        <input type="hidden" id="subscriber-source" class="form-control" name="Subscriber[source]" value="widget">

                                        <div class="help-block"></div>
                                    </div>

                                    <div class="form__item field-subscriber-email required">
                                        <input type="text" id="subscriber-email" class="subscribe-form__input" name="Subscriber[email]" maxlength="255" placeholder="укажите e-mail" autocomplete="off" aria-required="true"><div class="form__error-message"></div>
                                    </div>
                                    <button type="submit" class="button subscribe-form__button">Подписаться</button>
                                </form>
                                <div style="display:none">
                                    <div id="thank_you" class="popup popup_small">
                                        <div class="popup__holder">
                                            <h4 class="popup__head">Спасибо!</h4>
                                            <p>Мы будем сообщать вам о новинках.</p>
                                            <button type="button" class="button button_powdery featherlight-close">Продолжить</button>
                                        </div>
                                    </div>
                                </div>                </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="footer__line">
                <div class="footer__left">
                    <div class="reference-tools">

                        <div class="reference-tools__item">    	<a href="//en.12storeez.com/about" class="reference-tools__link en-US">
                                <i class="reference-tools__icon reference-tools__icon_lang"></i>English version		</a>
                        </div>          <div class="reference-tools__item"><a href="/pages/otzyvy" class="reference-tools__link"><i class="reference-tools__icon reference-tools__icon_reviews"></i>Отзывы</a></div>
                    </div>
                </div>
                <div class="footer__middle">
                    <div class="social-icons">
                        <div class="social-icons__item instagram"><a href="http://instagram.com/12Storeez" class="social-icons__link" target="_blank">instagram<i class="social-icons__icon social-icons__icon_instagram"></i></a></div>
                        <div class="social-icons__item facebook"><a href="https://www.facebook.com/12StoreezShop" class="social-icons__link" target="_blank">facebook<i class="social-icons__icon social-icons__icon_facebook"></i></a></div>
                        <div class="social-icons__item vkontakte"><a href="http://vk.com/12storeezshop" class="social-icons__link" target="_blank">vkontakte<i class="social-icons__icon social-icons__icon_vkontakte"></i></a></div>
                    </div>
                </div>
                <div class="footer__right">
                    <div class="reference-tools">
                        <div class="reference-tools__item"><a id="shops-link" href="/pages/shops" class="reference-tools__link"><i class="reference-tools__icon reference-tools__icon_stores"></i>Магазины</a></div>
                        <div class="reference-tools__item"><a id="contacts-link" href="/pages/contacts" class="reference-tools__link"><i class="reference-tools__icon reference-tools__icon_contacts"></i>Контакты</a></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>