<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<?= (isset($this->params['template']) && strlen($this->params['template']) > 0 )? "<html lang=\"ru-RU\" class=\"{$this->params['template']}\">" : "<html lang=\"ru-RU\">" ?>
<head>

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-57589TW');</script>

    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, minimal-ui">

    <title><?= Html::encode($this->title) ?></title>
    <meta property="og:title" content="<?= Html::encode($this->title) ?>">
    <meta name="twitter:title" content="<?= Html::encode($this->title) ?>">
    <meta name="description" content="">
    <meta property="og:description" content="">
    <meta name="twitter:description" content="">

    <meta name="theme-color" content="#ccb9aa">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="/assets/b282cc9/images/fav/12Storeez_fav_16.ico" rel="icon" type="image/x-icon">
    <link href="/assets/b282cc9/images/fav/12Storeez_fav_16.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="apple-touch-icon" href="/assets/b282cc9/images/fav/12Storeez_fav_1024.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/b282cc9/images/fav/12Storeez_fav_57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/b282cc9/images/fav/12Storeez_fav_60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/b282cc9/images/fav/12Storeez_fav_72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/b282cc9/images/fav/12Storeez_fav_76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/b282cc9/images/fav/12Storeez_fav_114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/b282cc9/images/fav/12Storeez_fav_120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/b282cc9/images/fav/12Storeez_fav_144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/b282cc9/images/fav/12Storeez_fav_152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/b282cc9/images/fav/12Storeez_fav_180.png">
    <link rel="icon" type="image/png" href="/assets/b282cc9/images/fav/12Storeez_fav_32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/assets/b282cc9/images/fav/12Storeez_fav_192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/assets/b282cc9/images/fav/12Storeez_fav_96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/assets/b282cc9/images/fav/12Storeez_fav_16.png" sizes="16x16">
    <link rel="mask-icon" href="/assets/b282cc9/images/safari-pinned-tab-32.svg" color="#c10e00">
    
    <meta name="yandex-verification" content="94ec2b4e7fdd4307">
    <meta property="fb:app_id" content="1840297862925714">

    <meta name="twitter:image" content="https://12storeez.com/uploads/images/slider/17%2004%2007/%D0%B3%D0%BE%D1%80%D0%B8%D0%B7%D0%BE%D0%BD%D1%8205.jpg">
    <meta property="og:image" content="https://12storeez.com/uploads/images/slider/17%2004%2007/%D0%B3%D0%BE%D1%80%D0%B8%D0%B7%D0%BE%D0%BD%D1%8205.jpg">
    <meta itemprop="image" content="https://12storeez.com/uploads/images/slider/17%2004%2007/%D0%B3%D0%BE%D1%80%D0%B8%D0%B7%D0%BE%D0%BD%D1%8205.jpg">
    <meta property="og:url" content="https://12storeez.com/">

    <link href="https://12storeez.com/" rel="canonical">
    <?= $this->registerCssFile("/assets/css-compress/c99674dd507a73d9cb5eaff3cfe48284.css?v=1492004349"); ?>
    <?= $this->registerCssFile("/css/style.css?v=1493982810"); ?>
     <script>
        var assetsUrl = '/assets/b282cc9';
    </script>
    <script src='//www.google.com/recaptcha/api.js?hl=ru' async defer></script>
    <?php $this->head() ?>
</head>
<body>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57589TW" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<nav class="menu">
    <div class="menu__middle">
        <div class="menu__holder">
            <div class="menu__list">
                <div class="menu__column">
                    <div id="catalog-section" class="menu__item menu__item_sub-opened">
                        <a href="#" class="menu__link menu__link_with-sub js-sub-menu"><span class="menu__text">Каталог<i class="menu__sub-icon"></i></span></a>
                        <ul class="menu__sub menu__sub_opened">

                            <li class="menu__sub-item">
                                <a href="/catalog/obuv" class="menu__sub-link">
                                    <span class="menu__text">обувь</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/plata" class="menu__sub-link">
                                    <span class="menu__text">платья</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/verhnaa-odezda" class="menu__sub-link">
                                    <span class="menu__text">верхняя одежда</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/topy-i-bluzy" class="menu__sub-link">
                                    <span class="menu__text">топы и блузы</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/trikotaz" class="menu__sub-link">
                                    <span class="menu__text">трикотаж</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/rubaski" class="menu__sub-link">
                                    <span class="menu__text">рубашки</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/ubki" class="menu__sub-link">
                                    <span class="menu__text">юбки</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/bruki" class="menu__sub-link">
                                    <span class="menu__text">брюки</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/komplekty" class="menu__sub-link">
                                    <span class="menu__text">комплекты</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/zakety" class="menu__sub-link">
                                    <span class="menu__text">жакеты</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/aksessuary" class="menu__sub-link">
                                    <span class="menu__text">аксессуары</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog/skoro-v-prodaze" class="menu__sub-link">
                                    <span class="menu__text">скоро в продаже</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/catalog" class="menu__sub-link">
                                    <span class="menu__text">посмотреть все</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="menu__column">
                    <div class="menu__item ">
                        <a href="/catalog/new" class="menu__link ">
                        <span class="menu__text">
                        NEW </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item menu__item_important">
                        <a href="/catalog/skidki" class="menu__link ">
                        <span class="menu__text">
                        ВЫБОР НЕДЕЛИ </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="/catalog/skoro-v-prodaze" class="menu__link ">
                        <span class="menu__text">
                        СКОРО В ПРОДАЖЕ </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="/lookbook" class="menu__link ">
                        <span class="menu__text">
                        Lookbook </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="/streetstyle" class="menu__link ">
                        <span class="menu__text">
                        Streetstyle </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="#" class="menu__link js-sub-menu">
                        <span class="menu__text">
                        ПОКУПАТЕЛЯМ <i class="menu__sub-icon"></i>
                        </span>
                        </a>
                        <ul class="menu__sub">
                            <li class="menu__sub-item">
                                <a href="/pages/rukovodstvo-po-pokupke" class="menu__sub-link">
                                    <span class="menu__text">руководство по покупке</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/oplata" class="menu__sub-link">
                                    <span class="menu__text">оплата</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/dostavka" class="menu__sub-link">
                                    <span class="menu__text">доставка</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/samovyvoz" class="menu__sub-link">
                                    <span class="menu__text">самовывоз</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/besplatnyiy-vozvrat" class="menu__sub-link">
                                    <span class="menu__text">бесплатный возврат</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/faq" class="menu__sub-link">
                                    <span class="menu__text">часто задаваемые вопросы (F.A.Q.)</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/kacestvo" class="menu__sub-link">
                                    <span class="menu__text">качество обслуживания</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/politika-konfidencialnosti" class="menu__sub-link">
                                    <span class="menu__text">политика конфиденциальности</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/oferta" class="menu__sub-link">
                                    <span class="menu__text">оферта</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="menu__item media-query-show_mobile"><a href="/pages/otzyvy" class="menu__link"><span class="menu__text">Отзывы</span></a></div>
                </div>
                <div class="menu__column">
                    <div class="menu__item ">
                        <a href="/pages/stories" class="menu__link ">
                        <span class="menu__text">
                        истории </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="/pages/shops" class="menu__link ">
                        <span class="menu__text">
                        МАГАЗИНЫ </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="/pages/contacts" class="menu__link ">
                        <span class="menu__text">
                        контакты </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="/video" class="menu__link ">
                        <span class="menu__text">
                        video </span>
                        </a>
                        <ul class="menu__sub">
                        </ul>
                    </div>
                    <div class="menu__item ">
                        <a href="#" class="menu__link js-sub-menu">
                        <span class="menu__text">
                        О КОМПАНИИ <i class="menu__sub-icon"></i>
                        </span>
                        </a>
                        <ul class="menu__sub">
                            <li class="menu__sub-item">
                                <a href="/pages/brend" class="menu__sub-link">
                                    <span class="menu__text">о нас</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/office" class="menu__sub-link">
                                    <span class="menu__text">офис</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/press" class="menu__sub-link">
                                    <span class="menu__text">пресса о нас</span>
                                </a>
                            </li>
                            <li class="menu__sub-item">
                                <a href="/pages/rabota-u-nas" class="menu__sub-link">
                                    <span class="menu__text">работа у нас</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="menu__column menu__column_desktop">
                    <div class="menu__item">
                        <div class="search menu__search-item">
                            <form id="search" class="search__form" method="get" action="/catalog">
                                <input type="text" name="search" placeholder="найти на сайте" autocomplete="off" class="search__input" value="" />
                                <button type="submit" class="search__button"><i class="search__icon"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="menu__item"><a href="/user/login" class="menu__link js-popup"><span class="menu__text">Войти в личный кабинет</span></a></div>
                    <div class="menu__item"><a href="/pages/otzyvy" class="menu__link"><span class="menu__text">Отзывы</span></a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu__bottom">
        <div class="menu__holder">
            <div class="menu__tools">
                <div class="menu__tools-item">
                    <a href="/user/wishlist" class="menu__tools-button menu__tools-button_favorite">
                        <span class="menu__count" style="display: none">0</span> </a>
                </div>
                <div class="menu__tools-item">
                    <a href="/user/login" class="menu__tools-button menu__tools-button_user"></a>
                </div>
                <div class="menu__tools-item">
                    <button type="button" form="search-mobile" data-wrapper=".menu__seach-wrapper" class="menu__tools-button menu__tools-button_search js-search-button"></button>
                </div>
            </div>
            <div class="menu__seach-wrapper">
                <div class="search">
                    <form id="search-mobile" class="search__form" method="get" action="/catalog">
                        <input type="text" name="search" placeholder="найти на сайте" autocomplete="off" class="search__input" value="" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</nav>
<?= $content ?>

<script>
    var serviceDomain = 'tracker.directcrm.ru';
    (function (window, document, script, url, objectName) {
        window[objectName] = window[objectName] || function ()
            {
                (window[objectName].Queue = window[objectName].Queue || []).push(arguments);
            },
            a = document.createElement(script),
            m = document.getElementsByTagName(script)[0]; a.async = 1; a.src = url + '?v=' + Math.random(); m.parentNode.insertBefore(a, m);
    })(window, document, 'script', 'https://' + serviceDomain + '/scripts/v1/tracker.js', 'directCrm');
    directCrm('create', {
        projectSystemName: '12storeez',
        brandSystemName: '12storeez',
        pointOfContactSystemName: '12storeez.com',
        projectDomain: '12storeez-services.directcrm.ru',
        serviceDomain: serviceDomain
    });
</script>
<script>
</script>


<script>
    var dataLayer = dataLayer || [];
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-63625959-1', 'auto');
    ga('send', 'pageview');
</script>


<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter30397872 = new Ya.Metrika({
                    id:30397872,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/30397872" style="position:absolute; left:-9999px;" alt="" /></div></noscript>


<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '247205632397185'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=247205632397185&ev=PageView&noscript=1"
    /></noscript>


<script>
    (function(w, d, s, h, id) {
        w.roistatProjectId = id; w.roistatHost = h;
        var p = d.location.protocol == "https:" ? "https://" : "http://";
        var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
        var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
    })(window, document, 'script', 'cloud.roistat.com', '383ed622501e83b2097eab55bc9603be');
</script>
<script src="/assets/js-compress/b8a7d467b23e03b3eab138f1052d73c4.js?v=1492004349"></script>
<script type="text/javascript">jQuery(document).ready(function () {
        $('.main .full-page a.button.full-page__button').click(function(e){ga('send','event','Main menu','click on the link banner');});
        window._retag=window._retag||[];window._retag.push({code:"9ce8887ee5",level:0});(function(){var id="admitad-retag";if(document.getElementById(id)){return;}
            var s=document.createElement("script");s.async=true;s.id=id;var r=(new Date).getDate();s.src=(document.location.protocol=="https:"?"https:":"http:")+"//cdn.lenmit.com/static/js/retag.js?r="+r;var a=document.getElementsByTagName("script")[0]
            a.parentNode.insertBefore(s,a);})();
        jQuery('#subscribe_form').yiiActiveForm([{"id":"subscriber-email","name":"email","container":".field-subscriber-email","input":"#subscriber-email","error":".form__error-message","validate":function(attribute,value,messages,deferred,$form){yii.validation.string(value,messages,{"message":"Значение «Email» должно быть строкой.","max":255,"tooLong":"Значение «Email» должно содержать максимум 255 символов.","skipOnEmpty":1});yii.validation.required(value,messages,{"message":"Необходимо заполнить «Email»."});yii.validation.email(value,messages,{"pattern":/^[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/,"fullPattern":/^[^@]*<[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?>$/,"allowName":false,"message":"Значение «Email» не является правильным email адресом.","enableIDN":false,"skipOnEmpty":1});}}],[]);
        $('a.lang-link.en-US').click(function(e){ga('send','event','Main menu','click on the link to the English version');});
        $('.social-icons__item.instagram>a').click(function(e){ga('send','event','Main menu','click on the link instagram');});$('.social-icons__item.facebook>a').click(function(e){ga('send','event','Main menu','click on the link Fb');});$('.social-icons__item.vkontakte>a').click(function(e){ga('send','event','Main menu','click on the link VK');});$('.footer__right a#shops-link').click(function(e){ga('send','event','Main menu','click the link stores');});$('.footer__right a#contacts-link').click(function(e){ga('send','event','Main menu','click on the contacts');});
        $('.menu-button.js-menu-button').click(function(e){ga('send','event','Main menu','click on the main menu');});$('.header__right-panel a.basket-button').click(function(e){ga('send','event','Main menu','click on the shopping cart');});
        $('#catalog-section .menu__sub-item').click(function(e){ga('send','event','Main menu','click on section catalog',$(this).find('span').text());});$('.menu .menu__column .menu__item a.menu__link').click(function(e){ga('send','event','Main menu','click on section',$(this).text());});
    });</script>
<svg viewBox="0 0 195 35" style="display: none">
    <symbol id="logo">
        <path d="M194.441,10.406H176.62v8.515h0.234c0-3.376,1.646-8.276,7.146-8.276h6.093L176.39,33.887v0.171H195 v-8.854h-0.232c0,3.718-1.642,8.615-7.146,8.615h-7.018l13.837-23.242V10.406z M161.27,22.063c4.977,0,5.47,2.737,5.47,6.384h0.229 V15.441h-0.229c0,3.583-0.493,6.384-5.106,6.384h-0.791V10.645h4.051c5.499,0,7.151,4.9,7.151,8.276h0.229v-8.515h-18.515v0.238 h3.296v23.175h-3.296v0.238h18.844v-8.854h-0.234c0,3.718-1.642,8.615-7.145,8.615h-4.382V22.063H161.27z M138.805,22.063 c4.977,0,5.47,2.737,5.47,6.384h0.23V15.441h-0.23c0,3.583-0.493,6.384-5.106,6.384h-0.791V10.645h4.051 c5.503,0,7.15,4.9,7.15,8.276h0.23v-8.515h-18.518v0.238h3.297v23.175h-3.297v0.238h18.845v-8.854h-0.23 c0,3.718-1.646,8.615-7.147,8.615h-4.381V22.063H138.805z M117.194,10.645c3.294,0,4.576,2.464,4.576,5.844 c0,3.345-1.282,5.88-4.576,5.88h-4.083V10.645H117.194z M129.318,30.441h-0.201c-0.621,2.974-1.708,3.277-2.205,3.277 c-3.522,0,0.859-10.608-8.136-11.251c4.381-0.404,6.786-2.535,6.786-5.979c0-3.988-2.966-6.083-8.368-6.083h-11.167v0.238h3.294 v23.175h-3.294v0.238h10.378v-0.238h-3.294V22.535h4.348c6.521,0,0.988,11.961,7.546,11.961 C126.682,34.496,128.622,34.157,129.318,30.441 M91.6,34.598c-6.028,0-7.312-4.494-7.312-12.265S85.572,10.07,91.6,10.07 s7.311,4.493,7.311,12.263S97.628,34.598,91.6,34.598 M91.6,9.833c-6.325,0-11.462,5.271-11.462,12.5 c0,7.229,5.138,12.5,11.462,12.5s11.464-5.271,11.464-12.5C103.064,15.104,97.925,9.833,91.6,9.833 M70.122,10.645 c5.502,0,7.148,4.9,7.148,8.951h0.232v-9.189H58.527v9.189h0.232c0-4.051,1.646-8.951,7.147-8.951h0.231v23.175h-3.294v0.238h10.376 v-0.238h-3.293V10.645H70.122z M44.596,34.717c-6.349,0-10.532-6.51-10.532-13.632h-0.37v12.973h5.75 C41.008,34.671,42.709,35,44.596,35c7.542,0,11.039-5.378,11.039-10.566c0-12.218-18.672-7.973-18.672-17.737 c0-3.159,3.034-6.415,7.083-6.415c6.623,0,9.151,6.136,9.151,12.218h0.369V0l-4.738,0.99C47.447,0.329,45.929,0,44.045,0 c-4.555,0-9.292,4.054-9.292,9.292c0,13.303,18.305,8.114,18.305,18.492C53.058,31.322,49.702,34.717,44.596,34.717 M19.863,13.82 c0-1.218-1.02-2.266-2.206-2.266c-0.262,0-0.527,0.07-0.79,0.169c0.954-1.113,2.273-1.756,3.721-1.756 c2.801,0,4.052,1.926,4.052,4.932c0,3.617-2.371,9.124-5.895,13.313l-4.579,5.472h0.031l-0.031,0.034v0.339H29.35V26.22h-0.229 c0,3.378-0.66,4.628-3.493,4.628h-8.663l2.207-2.535c5.139-5.943,9.422-8.717,9.422-13.107c0-3.547-3.524-5.542-7.083-5.542 c-2.471,0-6.029,1.586-6.029,4.157c0,1.183,1.022,2.23,2.175,2.23C18.843,16.05,19.863,15.003,19.863,13.82 M0,12.535h3.296v21.284 H0v0.238h10.377v-0.238H7.083V9.732H6.786L3.36,12.299H0V12.535z"></path>
    </symbol>
</svg>
<script type='text/javascript'>
    window['li'+'ve'+'Tex'] = true,
        window['liv'+'eT'+'exI'+'D'] = 102605,
        window['li'+'ve'+'Tex_o'+'bject'] = true;
    (function() {
        var t = document['cr'+'eate'+'Elem'+'ent']('script');
        t.type ='text/javascript';
        t.async = true;
        t.src = '//cs15.livet'+'ex.ru/'+'js/client.js';
        var c = document['g'+'etE'+'leme'+'ntsByTa'+'gName']('script')[0];
        if ( c ) c['paren'+'tNod'+'e']['i'+'nsertB'+'efore'](t, c);
        else document['d'+'o'+'c'+'umentE'+'lement']['fi'+'rs'+'t'+'Ch'+'ild']['app'+'en'+'dChi'+'l'+'d'](t);
    })();
</script>
<script type="text/javascript">
    (function (atm, doIt) {
// atm.order_match = '546'; // OPTIONAL: Order MATCH
// atm.order_value = '19.99'; // OPTIONAL: Order value
// atm.product_id = 'product-id'; // OPTIONAL: Product name or product ID
// Do not modify any of the code below.
        atm.client_id = "58b5b11750f18471c94cb0b3a6f336ca"; // CLIENT: d.merkel@12storeez.com
        doIt(atm);
    })({}, function(d){var a=document.createElement("iframe");a.style.cssText="width: 0; height: 0; border: 0; position: absolute; left: -5px;";a.src="javascript:false";var c=function(){setTimeout(function(){var c=a.contentDocument||a.contentWindow.document,b=c.createElement("script");b.src="//static-trackers.adtarget.me/javascripts/pixel.min.js";b.id="GIHhtQfW-atm-pixel";b["data-pixel"]=d.client_id;b["allow-flash"]=d.allow_flash;b.settings_obj=d;c.body.appendChild(b)},0)};a.addEventListener?a.addEventListener("load",c,!1):a.attachEvent?a.attachEvent("onload",c):a.onload=c;document.body.appendChild(a)});
    // After the pixel above is loaded, you can use custom AJAX calls to register custom events (like button clicks, form fillouts and etc. )
    //_AtmUrls = window._AtmUrls || [];
    //__AtmUrls.push('add-to-cart');
</script>
</body>
</html>
<?php $this->endPage() ?>
