<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\GeneralPage;
use common\models\Pages;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => 'yii\web\ViewAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAjax(){
        $request = Yii::$app->request;
        $controller = $request->post('controller');
        $action = $request->post('action');
        $id = $request->post('id');

        switch ($controller){
            case 'page_general':
//                $item = GeneralPage::find()->where("id = $id")->one();
                if($action == 'activeIt'){
                    Yii::$app->db->createCommand("UPDATE `general_page` SET `active` = 1 WHERE `id`=" . $id)->execute();
                    echo 'activeIt_'.$id;
                    break;
                }
                if($action == 'deactiveIt'){
                    Yii::$app->db->createCommand("UPDATE `general_page` SET `active` = 0 WHERE `id`=" . $id)->execute();
                    echo 'deactiveIt_'.$id;
                    break;
                }
                if($action == 'deleteIt'){
                    Yii::$app->db->createCommand("DELETE FROM `general_page` WHERE `id`=" . $id)->execute();
                    break;
                }
                break;

            case 'pages':
                if($action == 'activeIt'){
                    Yii::$app->db->createCommand("UPDATE `pages` SET `active` = 1 WHERE `id`=" . $id)->execute();
                    echo 'activeIt_'.$id;
                    break;
                }
                if($action == 'deactiveIt'){
                    Yii::$app->db->createCommand("UPDATE `pages` SET `active` = 0 WHERE `id`=" . $id)->execute();
                    echo 'deactiveIt_'.$id;
                    break;
                }
                if($action == 'deleteIt'){
                    Yii::$app->db->createCommand("DELETE FROM `general_page` WHERE `id`=" . $id)->execute();
                    break;
                }
                if($action == 'pageEdit') {
                    $requestData = json_decode($request->post('requestData'), true);

                    if (isset($requestData)) {
                        $params = '';
                        foreach ($requestData as $key => $value) {
                            $params .= " `{$key}` = '{$value}',";
                        }
                        $params = substr($params, 0, -1);
                        Yii::$app->db->createCommand("UPDATE `pages` SET {$params} WHERE `id`='{$id}'")->execute();
                        echo 'pageEditOk';
                        break;
                    }else{
                        echo 'no isset requestData';
                        break;
                    }
                }
                if($action == 'pageAdd') {
                    $requestData = json_decode($request->post('requestData'), true);

                    if (isset($requestData)) {
                        $tables = '';
                        $values = '';
                        foreach ($requestData as $key => $value) {
                            $tables .= " `{$key}`,";
                            $values .= "'{$value}',";
                        }
                        $tables = substr($params, 0, -1);
                        $values = substr($params, 0, -1);
                        Yii::$app->db->createCommand("INSERT INTO `pages` ({$tables}) VALUES ({$values})")->execute();
                        echo 'pageAddOk';
                        break;
                    }else{
                        echo 'no isset requestData';
                        break;
                    }
                }
                break;

            default:
                echo '<pre>'.print_R($request,true).'</pre>';
                break;
        }
    }

    public function actionPages()
    {
        if(isset($_GET['add'])){
            return $this->render('pages_add');
        }
        else if(isset($_GET['edit'])){
            if($_GET['edit'] == 1){
                $lines = GeneralPage::find()->orderBy('position')->all();
                $page = Pages::find()->where('id = 1')->one();
                return $this->render('page_general', [
                    'lines' => $lines,
                    'page' => $page,
                ]);
            }
            $page = Pages::find()->where(['id' => $_GET['edit']])->one();
            return $this->render('pages_edit', [
                'page' => $page,
            ]);
        }
        $lines = Pages::find()->orderBy('id')->all();
        return $this->render('pages', [
            'lines' => $lines,
        ]);
    }

    public function actionPage_general()
    {
        $lines = GeneralPage::find()->orderBy('position')->all();
        $page = Pages::find()->where('id = 1')->one();
        return $this->render('page_general', [
            'lines' => $lines,
            'page' => $page,
        ]);
    }

    public function actionPages_categories(){
        return $this->render('pages_categories');
    }

    public function actionMenus()
    {
        return $this->render('menus');
    }

    public function actionModules()
    {
        if(isset($_GET['type'])){
            if(isset($_GET['part'])){
                return $this->render("modules_{$_GET['type']}_{$_GET['part']}");
            }
            return $this->render("modules_{$_GET['type']}");
        }
        return $this->render('modules');
    }

    public function actionUsers()
    {
        return $this->render('users');
    }

    public function actionSettings()
    {
        return $this->render('settings');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
