<?php
/* @var $this yii\web\View */

$this->title = 'Главная страница сайта - WebArt CMS';
?>
<div class="page_lmenu">
    <div class="left_menu">
        <ul class="menu">
            <li><a href="pages">Информационные страници</a></li>
            <li class="active">Главная страница</li>
            <li><a href="pages_categories">Категории</a></li>
            <li class="disabled"><a href="#">Пусто</a></li>
            <li class="disabled"><a href="#">Пусто</a></li>
        </ul>
    </div>
    <div class="content general_page">
        <h1>Главная страница</h1>
        <div class="buttonline right">
            <div class="item">
                <button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>Добавить слайд</button>
            </div>
        </div>
        <div class="cols">
            <div class="left_col">
                <div class="block">
                    <h3>Слайдшоу</h3>
                    <div class="lines">
                        <table>
                            <tr class="line">
                                <th>Id</th>
                                <th>Название</th>
                                <th>Фон</th>
                                <th></th>
                            </tr>
                            <?php foreach ($lines as $line) {?>
                                <tr class="line cline prev" id="prev_<?= $line->id ?>">
                                    <td class="editopen"><?= $line->id ?></td>
                                    <td class="editopen"><?= $line->name ?></td>
                                    <td class="editopen"><img src="<?= $line->image ?>" alt="" /></td>
                                    <td><span class="btn button_active glyphicon <?= ($line->active == 1) ? 'glyphicon-eye-open' : 'glyphicon-eye-close' ?>"></span>
                                        <span class="btn button_delete glyphicon glyphicon-remove"></span>
                                    </td>
                                </tr>
                                <tr class="line cline none" id="edit_<?= $line->id ?>">
                                    <td colspan="4">
                                        <div><input class="inline_center" type="text" value="<?= $line->name ?>"></div>
                                        <div><input class="inline_center" type="file" value="image_<?= $line->image ?>"></div>
                                        <div><textarea class="inline_center" cols="30" rows="10" ><?= $line->content ?></textarea></div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="right_col">
                <div class="block meta">
                    <h3>Мета теги</h3>
                    <div class="lines">
                        <div class="line">
                            <span class="title">Заголовок</span>
                            <input type="text" placeholder="Title" value="<?= $page->title ?>">
                        </div>
                        <div class="line">
                            <span class="title">Описание</span>
                            <input type="text" placeholder="Description" value="<?= $page->description ?>">
                        </div>
                        <div class="line">
                            <span class="title">Ключевые слова</span>
                            <input type="text" placeholder="Keywords" value="<?= $page->keywords ?>">
                        </div>
                        <div class="line">
                            <span class="title">Индексация</span>
                            <select name="robots" >
                                <option value="if" selected="selected">Index, Follow</option>
                                <option value="in">Index, Nofollow</option>
                                <option value="nf">Noindex, Follow</option>
                                <option value="nn">Noindex, Nofollow</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttonline center">
            <div class="item">
                <button class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span>Сохранить</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('.prev td.editopen').click(function(){
        var id = $(this).closest('tr').attr('id').substr(5);
        if( $('#edit_'+id).is(":visible") == true){
            $('#edit_'+id).hide(500);
        }else{
            $('#edit_'+id).show(800);
        }
    });
</script>
<script>
    $('.button_active').click(function(){
        var id = $(this).closest('tr').attr('id').substr(5);
        var classBtn = $(this).attr('class');
        if(classBtn == 'btn button_active glyphicon glyphicon-eye-close'){
            $(this).removeClass('glyphicon-eye-close');
            $.ajax({
                type:'get',//тип запроса: get,post либо head
                url: '/admin/ajax',//url адрес файла обработчика
                data:{'controller':'page_general', 'action':'activeIt', 'id':id},//параметры запроса
                response:'text',//тип возвращаемого ответа text либо xml
                success:function (data) {//возвращаемый результат от сервера
                    if(data == 'activeIt_'+id){
                        $('#prev_'+id+' .button_active').addClass('glyphicon-eye-open');
                    }else{
                        $('#prev_' + id + ' .button_active').addClass('glyphicon-eye-close');
                    }
                }
            });
        }
        if(classBtn == 'btn button_active glyphicon glyphicon-eye-open'){
            $(this).removeClass('glyphicon-eye-open');
            $.ajax({
                type:'get',//тип запроса: get,post либо head
                url: '/admin/ajax',//url адрес файла обработчика
                data:{'controller':'page_general', 'action':'deactiveIt', 'id':id},//параметры запроса
                response:'text',//тип возвращаемого ответа text либо xml
                success:function (data) {//возвращаемый результат от сервера
                    if(data == 'deactiveIt_'+id) {
                        $('#prev_' + id + ' .button_active').addClass('glyphicon-eye-close');
                    }else{
                        $('#prev_'+id+' .button_active').addClass('glyphicon-eye-open');
                    }
                }
            });
        }
    });
</script>
<script>
    $('.button_delete').click(function(){
        var id = $(this).closest('tr').attr('id').substr(5);
        var isConfirm = confirm("Вы уверены что хотите удалить этот слайд?");
        if(isConfirm == true){
            $.ajax({
                type:'get',//тип запроса: get,post либо head
                url: '/admin/ajax',//url адрес файла обработчика
                data:{'controller':'page_general', 'action':'deleteIt', 'id':id},//параметры запроса
                response:'text',//тип возвращаемого ответа text либо xml
                success:function (data) {//возвращаемый результат от сервера
                    $('#prev_'+id).hide(500);
                }
            });
        }
    });
</script>