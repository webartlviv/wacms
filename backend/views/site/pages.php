<?php
/* @var $this yii\web\View */

$this->title = 'Страници сайта - WebArt CMS'; ?>
    <div class="page_lmenu">
        <div class="left_menu">
            <ul class="menu">
                <li class="active">Информационные страницы</li>
                <li><a href="page_general">Главная страница</a></li>
                <li><a href="pages_categories">Категории</a></li>
                <li class="disabled"><a href="#">Пусто</a></li>
                <li class="disabled"><a href="#">Пусто</a></li>
            </ul>
        </div>
        <div class="content">
            <h1>Страницы</h1>
            <div class="buttonline right">
                <div class="item">
                    <a href="pages?add"><button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>Добавить</button></a>
                </div>
            </div>
            <div class="fullcontent">
                <div class="block">
                    <div class="lines">
                        <table>
                            <tr class="line">
                                <th>Id</th>
                                <th>Название</th>
                                <th>Алиас</th>
                                <th>Категория</th>
                                <th></th>
                            </tr>
                            <?php foreach ($lines as $line) {?>
                                <tr class="line cline prev" id="prev_<?= $line->id ?>" data-href="pages?edit=<?= $line->id ?>">
                                    <td class="editopen"><?= $line->id ?></td>
                                    <td class="editopen"><?= rawurldecode( $line->name ) ?></td>
                                    <td class="editopen"><?= rawurldecode( $line->alias ) ?></td>
                                    <td class="editopen"><?= $line->category_id ?></td>
                                    <td <?= ($line->id == 1) ? 'class="editopen"' : '' ?>>
                                        <?php if($line->id != 1) { ?>
                                            <span class="btn button_active glyphicon <?= ($line->active == 1) ? 'glyphicon-eye-open' : 'glyphicon-eye-close' ?>""></span>
                                            <span class="btn button_delete glyphicon glyphicon-remove"></span>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $('.prev td.editopen').click(function(){
        var id = $(this).closest('tr').attr('id').substr(5),
            href = $(this).closest('tr').data('href');
            window.location.href = href;
    });
</script>
<script>
    $('.button_active').click(function(){
        var id = $(this).closest('tr').attr('id').substr(5);
        var classBtn = $(this).attr('class');
        if(classBtn == 'btn button_active glyphicon glyphicon-eye-close'){
            $(this).removeClass('glyphicon-eye-close');
            $.ajax({
                type:'post',//тип запроса: get,post либо head
                url: '/admin/ajax',//url адрес файла обработчика
                data:{'controller':'pages', 'action':'activeIt', 'id':id},//параметры запроса
                response:'text',//тип возвращаемого ответа text либо xml
                success:function (data) {//возвращаемый результат от сервера
                    if(data == 'activeIt_'+id){
                        $('#prev_'+id+' .button_active').addClass('glyphicon-eye-open');
                    }else{
                        $('#prev_' + id + ' .button_active').addClass('glyphicon-eye-close');
                    }
                }
            });
        }
        if(classBtn == 'btn button_active glyphicon glyphicon-eye-open'){
            $(this).removeClass('glyphicon-eye-open');
            $.ajax({
                type:'post',//тип запроса: get,post либо head
                url: '/admin/ajax',//url адрес файла обработчика
                data:{'controller':'pages', 'action':'deactiveIt', 'id':id},//параметры запроса
                response:'text',//тип возвращаемого ответа text либо xml
                success:function (data) {//возвращаемый результат от сервера
                    if(data == 'deactiveIt_'+id) {
                        $('#prev_' + id + ' .button_active').addClass('glyphicon-eye-close');
                    }else{
                        $('#prev_'+id+' .button_active').addClass('glyphicon-eye-open');
                    }
                }
            });
        }
    });
</script>
<script>
    $('.button_delete').click(function(){
        var id = $(this).closest('tr').attr('id').substr(5);
        var isConfirm = confirm("Вы уверены что хотите удалить эту страницу?");
        if(isConfirm == true){
            $.ajax({
                type:'post',//тип запроса: get,post либо head
                url: '/admin/ajax',//url адрес файла обработчика
                data:{'controller':'pages', 'action':'deleteIt', 'id':id},//параметры запроса
                response:'text',//тип возвращаемого ответа text либо xml
                success:function (data) {//возвращаемый результат от сервера
                    $('#prev_'+id).hide(500);
                }
            });
        }
    });
</script>