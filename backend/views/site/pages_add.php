<?php
/* @var $this yii\web\View */

$this->title = 'Страници сайта - WebArt CMS'; ?>
<div class="page_lmenu">
    <div class="left_menu">
        <ul class="menu">
            <li class="active">Информационные страницы</li>
            <li><a href="page_general">Главная страница</a></li>
            <li><a href="pages_categories">Категории</a></li>
            <li class="disabled"><a href="#">Пусто</a></li>
            <li class="disabled"><a href="#">Пусто</a></li>
        </ul>
    </div>
    <div class="content">
        <h1>Добавить страницу</h1>
        <div class="buttonline left">
            <div class="item">
                <a href="pages"><button class="btn btn-primary"><span class="glyphicon glyphicon-chevron-left"></span>Назад</button></a>
            </div>
        </div>
        <div class="cols form">
            <div class="left_col">
                <div class="fullcontent container block_float">
                    <div class="lines">
                        <div class="line">
                            <div class="col-md-4 tright"><span class="title">Название</span></div>
                            <div class="col-md-8 tleft"><input type="text" placeholder="Name" name="name" value=""></div>
                        </div>
                        <div class="line">
                            <div class="col-md-4 tright"><span class="title">Алиас</span></div>
                            <div class="col-md-8 tleft"><input type="text" placeholder="Alias" name="alias" value=""></div>
                        </div>
                        <div class="line">
                            <div class="col-md-4 tright"><span class="title">Категория</span></div>
                            <div class="col-md-8 tleft"><select name="category_id">
                                    <option value="0">Служебная</option>
                                    <option value="1" selected="selected">Информационные страници</option>
                                </select>
                            </div>
                        </div>
                        <div class="line">
                            <div class="col-md-4 tright"><span class="title">Контент</span></div>
                            <div class="col-md-8 tleft"><textarea name="content" cols="30" rows="10"></textarea></div>
                        </div>
                        <div class="line">
                            <div class="col-md-4 tright"><span class="title">Шаблон</span></div>
                            <div class="col-md-8 tleft"><select name="template">
                                    <option value="0" selected="selected">Стандартный</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right_col">
                <div class="block meta">
                    <h3>Мета теги</h3>
                    <div class="lines">
                        <div class="line">
                            <span class="title">Заголовок</span>
                            <input type="text" placeholder="Title" name="title" value="">
                        </div>
                        <div class="line">
                            <span class="title">Описание</span>
                            <input type="text" placeholder="Description" name="description" value="">
                        </div>
                        <div class="line">
                            <span class="title">Ключевые слова</span>
                            <input type="text" placeholder="Keywords" name="keywords" value="">
                        </div>
                        <div class="line">
                            <span class="title">Индексация</span>
                            <select name="robots" name="robots">
                                <option value="Index, Follow" selected="selected">Index, Follow</option>
                                <option value="Index, Nofollow">Index, Nofollow</option>
                                <option value="Noindex, Follow">Noindex, Follow</option>
                                <option value="Noindex, Nofollow">Noindex, Nofollow</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttonline center">
            <div class="item">
                <button class="btn btn-success formSubmitButton"><span class="glyphicon glyphicon-floppy-disk"></span>Сохранить</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('.formSubmitButton').click(function(){
        var elements = $('.form').find('input, select'),
            textareas = $('.form').find('textarea'),
            request = '',
            buttonHtml = $(this).html();
        $(this).html('<span class="glyphicon glyphicon-refresh"></span>Отправка запроса');
        $(this).attr('disabled', 'true');

        if(textareas.length > 0){
            for (var i = 0; i < textareas.length; i++) {
                request += "\"" + $(textareas[i]).attr('name') + '" : "' +encodeURIComponent(  $(textareas[i]).val() ) + '", ';
            }
        }

        for (var i = 0; i < elements.length; i++) {
            if($(elements[i]).attr('type') == 'checkbox' || $(elements[i]).attr('type') == 'radio'){
                if($(elements[i]).is(":checked") == true){
                    request += "\"" + $(elements[i]).attr('name') + '" : "1", ';
                }
            }else{
                request += "\"" + $(elements[i]).attr('name') + '" : "' + encodeURIComponent( $(elements[i]).val() ) + '", ';
            }
        }
        request = '{'+request.substr(0, request.length-2)+'}';
        $.ajax({
            type:'post',//тип запроса: get,post либо head
            url: '/admin/ajax',//url адрес файла обработчика
            data:{'controller':'pages', 'action':'pageAdd', 'requestData':request},//параметры запроса
            response:'text',//тип возвращаемого ответа text либо xml
            success:function (data) {//возвращаемый результат от сервера
                if(data == 'pageAddOk'){
                    window.location.href = '/admin/pages';
                }else{
                    console.log(data);
                    alert('Случилась ошибка! Данные записаны в консоле!');
                    $('.formSubmitButton').html(buttonHtml);
                    $('.formSubmitButton').prop('disabled', false);
                }
            }
        });
    });
</script>
