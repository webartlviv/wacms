<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Модули сайта - WebArt CMS';
?>
<div class="page_lmenu">
    <div class="left_menu">
        <ul class="menu">
            <li><a href="/admin/modules?type=shop">Интернет-магазин</a></li>
            <li><a href="/admin/modules?type=blog">Блог</a></li>
            <li class="disabled"><a href="#">Пусто</a></li>
            <li class="disabled"><a href="#">Пусто</a></li>
            <li class="disabled"><a href="#">Пусто</a></li>
        </ul>
    </div>
    <div class="content">
        <h1>Модули</h1>
        <div class="bigMenuButton"><a href="/admin/modules?type=shop">Интернет-магазин</a></div>
        <div class="bigMenuButton"><a href="/admin/modules?type=blog">Блог</a></div>
    </div>
</div>
